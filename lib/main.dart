import 'package:flutter/material.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Home - First page"),
        ),
        body:
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // const FlutterLogo(size: 50,),
            const CircleAvatar(
              backgroundImage: AssetImage('images/pro1.jpg'),
              radius: 50,
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: const [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Weerphat", style: TextStyle(fontSize: 20),),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Deverloper", style: TextStyle(fontSize: 20),),
                ),
              ],
            ),
          ],
        ),
        // Center(
        //   child:
        //   Text("Hello My App",
        //   style: TextStyle(
        //     fontSize: 35,
        //     fontWeight: FontWeight.bold,
        //   ),
        //   ),
        // ),
      ),
      theme: ThemeData(
        primarySwatch: Colors.teal,
        scaffoldBackgroundColor: Colors.teal[100],
      ),
    );
  }
}
